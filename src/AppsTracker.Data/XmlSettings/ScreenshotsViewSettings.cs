﻿
namespace AppsTracker.Data.XmlSettings
{
    public sealed class ScreenshotsViewSettings : XmlSettingsBase
    {
        [SettingsNode]
        public double SeparatorPosition { get; set; }
    }
}
