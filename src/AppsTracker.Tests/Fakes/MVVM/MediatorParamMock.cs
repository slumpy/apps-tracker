﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppsTracker.Tests.Fakes.MVVM
{
   public class MediatorParamMock
    {
        public string Message { get; set; }
    }
}
